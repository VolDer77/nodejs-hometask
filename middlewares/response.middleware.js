const responseMiddleware = (req, res, next) => {
  // TODO: Implement middleware that returns result of the query
  try {
    const data = req.body;
    if (data && Object.values(data)) {
      res.status(200).json(data);
      return;
    }
  } catch (error) {
    if (
      req.method === "PUT" ||
      req.method === "DELETE" ||
      req.method === "POST"
    ) {
      res.status(400).json({
        error: true,
        message: `${error}`,
      });
      return;
    }
    res.status(404).json({
      error: true,
      message: `${error}`,
    });
    next();
  } finally {
    return;
  }
};

exports.responseMiddleware = responseMiddleware;
