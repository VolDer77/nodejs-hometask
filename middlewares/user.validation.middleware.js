const { user } = require("../models/user");
const { validate } = require("../validation/validation");
const { isEqual } = require("../utils/isEqual");

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  const errors = validate("user", req.body);
  const message = Object.values(errors).join(", ");

  if (errors) {
    res.status(400).json({
      error: true,
      message,
    });
    return;
  }

  if (!isEqual(user, req.body)) {
    res.status(400).json({
      error: true,
      message: "User entity to create isn't valid",
    });
    return;
  }

  next();
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  const errors = validate("user", req.body, req.method);
  const message = Object.values(errors).join(", ");

  if (errors) {
    res.status(400).json({
      error: true,
      message,
    });
    return;
  }

  if (!isEqual(user, req.body)) {
    res.status(400).json({
      error: true,
      message: "User entity to update isn't valid",
    });
    return;
  }

  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
