const { fighter } = require("../models/fighter");
const { validate } = require("../validation/validation");
const { isEqual } = require("../utils/isEqual");

const createFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during creation
  const errors = validate("fighter", req.body);
  const message = Object.values(errors).join(", ");

  if (errors) {
    res.status(400).json({
      error: true,
      message,
    });
    return;
  }

  if (!isEqual(fighter, req.body)) {
    res.status(400).json({
      error: true,
      message: "Fighter entity to create isn't valid",
    });
    return;
  }

  next();
};

const updateFighterValid = (req, res, next) => {
  // TODO: Implement validatior for fighter entity during update
  const errors = validate("fighter", req.body, req.method);
  const message = Object.values(errors).join(", ");

  if (errors) {
    res.status(400).json({
      error: true,
      message,
    });
    return;
  }

  if (!isEqual(fighter, req.body)) {
    res.status(400).json({
      error: true,
      message: "Fighter entity to update isn't valid",
    });
    return;
  }

  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
