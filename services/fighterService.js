const { FighterRepository } = require("../repositories/fighterRepository");
const { fighter } = require("../models/fighter");

class FighterService {
  // TODO: Implement methods to work with fighters
  getAllFighters() {
    const fighters = FighterRepository.getAll();
    if (!fighters) {
      return null;
    }
    return fighters;
  }

  createFighter(data) {
    if (!data.hasOwnProperty("health")) {
      data.health = fighter.health;
    }
    const createdFighter = FighterRepository.create(data);
    if (!createdFighter) {
      return null;
    }

    return createdFighter;
  }

  updateFighter(id, data) {
    if (!this.searchFighter({ id })) {
      return null;
    }

    const fighter = FighterRepository.update(id, data);
    if (!fighter) {
      return null;
    }
    return fighter;
  }

  deleteFighter(id) {
    const fighterToDelete = FighterRepository.delete(id);
    if (!fighterToDelete.length) {
      return null;
    }

    return fighterToDelete;
  }

  searchFighter(search) {
    const fighter = FighterRepository.getOne(search);
    if (!fighter) {
      return null;
    }
    return fighter;
  }
}

module.exports = new FighterService();
