const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user
  getAllUsers() {
    const users = UserRepository.getAll();
    if (!users) {
      return null;
    }
    return users;
  }

  createUser(data) {
    const user = UserRepository.create(data);
    if (!user) {
      return null;
    }

    return user;
  }

  updateUser(id, data) {
    if (!this.search({ id })) {
      return null;
    }

    const user = UserRepository.update(id, data);
    if (!user) {
      return null;
    }
    return user;
  }

  deleteUser(id) {
    const userToDelete = UserRepository.delete(id);
    if (!userToDelete.length) {
      return null;
    }

    return userToDelete;
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
