const { Router } = require("express");
const UserService = require("../services/userService");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.get(
  "/",
  (req, res) => {
    const users = UserService.getAllUsers();
    if (users) {
      res.status(200).json(users);
    } else {
      res.status(400).json({
        error: true,
        message: "Users not found",
      });
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res) => {
    const { id } = req.params;
    const user = UserService.search({ id });
    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).json({
        error: true,
        message: "User not found",
      });
    }
  },
  responseMiddleware
);

router.post(
  "/",
  createUserValid,
  (req, res) => {
    const data = req.body;
    const user = UserService.createUser(data);

    if (user) {
      res.status(200).json(user);
    } else {
      res.status(400).json({
        error: true,
        message: "Could not create user",
      });
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateUserValid,
  (req, res) => {
    const { id } = req.params;
    const data = req.body;
    const user = UserService.updateUser(id, data);
    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).json({
        error: true,
        message: "User not found with this id",
      });
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res) => {
    const { id } = req.params;
    const user = UserService.deleteUser(id);
    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).json({
        error: true,
        message: "User not found with this id",
      });
    }
  },
  responseMiddleware
);

module.exports = router;
