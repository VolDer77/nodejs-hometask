const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

router.post(
  "/login",
  (req, res, next) => {
    try {
      const data = req.body;
      const user = AuthService.login(data);

      if (user) {
        res.status(200).json(user);
        return;
      }
      // TODO: Implement login action (get the user if it exist with entered credentials)
    } catch (err) {
      res.status(404).json({
        error: true,
        message: "User not found",
      });
      return;
    } finally {
      next();
    }
  },
  responseMiddleware
);

module.exports = router;
