const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

// TODO: Implement route controllers for fighter

router.get(
  "/",
  (req, res) => {
    const fighters = FighterService.getAllFighters();
    if (fighters) {
      res.status(200).json(fighters);
    } else {
      res.status(400).json({
        error: true,
        message: "Fighters not found",
      });
    }
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res) => {
    const { id } = req.params;
    const fighter = FighterService.searchFighter({ id });
    if (fighter) {
      res.status(200).json(fighter);
    } else {
      res.status(404).json({
        error: true,
        message: "Fighter not found",
      });
    }
  },
  responseMiddleware
);

router.post(
  "/",
  createFighterValid,
  (req, res) => {
    const data = req.body;
    const fighter = FighterService.createFighter(data);

    if (fighter) {
      res.status(200).json(fighter);
    } else {
      res.status(400).json({
        error: true,
        message: "Could not create fighter",
      });
    }
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateFighterValid,
  (req, res) => {
    const { id } = req.params;
    const data = req.body;
    const fighter = FighterService.updateFighter(id, data);
    if (fighter) {
      res.status(200).json(fighter);
    } else {
      res.status(404).json({
        error: true,
        message: "Fighter not found with this id",
      });
    }
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res) => {
    const { id } = req.params;
    const fighter = FighterService.deleteFighter(id);
    if (fighter) {
      res.status(200).json(fighter);
    } else {
      res.status(404).json({
        error: true,
        message: "Fighter not found with this id",
      });
    }
  },
  responseMiddleware
);

module.exports = router;
