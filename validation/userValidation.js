const UserService = require("../services/userService");
const { REG_EXPS } = require("../utils/regExps");

function validateUser(params, method) {
  const { firstName, lastName, email, phoneNumber, password } = params;

  const errors = {};

  if (params.id) {
    errors.id = "ID is not required";
  }

  const existingEmail = UserService.search({ email });
  if (email && existingEmail && method !== "PUT") {
    errors.email = "User with this email already exists";
  }
  const existingPhoneNumber = UserService.search({ phoneNumber });
  if (phoneNumber && existingPhoneNumber && method !== "PUT") {
    errors.phoneNumber = "User with this phone number already exists";
  }

  if (!firstName) {
    errors.firstName = "First name is required";
  }

  if (!lastName) {
    errors.lastName = "Last name is required";
  }

  if (!email) {
    errors.email = "Email is required";
  }

  if (!phoneNumber) {
    errors.phoneNumber = "Phone number is required";
  }

  if (!password) {
    errors.password = "Password is required";
  }

  if (email && !REG_EXPS.GMAIL_REGEXP.test(email)) {
    errors.email = "Email isn't valid";
  }

  if (phoneNumber && !REG_EXPS.PHONE_NUMBER_REGEXP.test(phoneNumber)) {
    errors.phoneNumber = "Phone number isn't valid";
  }

  if (password && password.length < 3) {
    errors.password = "Password must include minimum 3 symbols";
  }

  return Object.values(errors).length ? errors : false;
}
exports.validateUser = validateUser;
