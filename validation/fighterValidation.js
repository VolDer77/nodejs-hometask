const FighterService = require("../services/fighterService");

function validateFighter(params, method) {
  const { name, health, power, defense } = params;

  const errors = {};

  let uniqueName;
  if (name) {
    uniqueName = name.toLowerCase();
  }

  const existingFighter = FighterService.searchFighter({ name: uniqueName });
  let existingName;
  if (existingFighter) {
    existingName = existingFighter.name.toString().toLowerCase();
  }
  if (name && existingName && existingName === uniqueName && method !== "PUT") {
    errors.name = "Fighter with this name already exists";
  }

  if (params.id) {
    errors.id = "ID is not required";
  }

  if (!name) {
    errors.name = "Name is required";
  }

  if (!power) {
    errors.power = "Power is required";
  }

  if (!defense) {
    errors.defense = "Defense is required";
  }

  if (
    (health && health < 80) ||
    health > 120 ||
    (health && isNaN(Number(health)))
  ) {
    errors.health = "Health isn't valid";
  }

  if ((defense && defense < 1) || defense > 10 || isNaN(Number(defense))) {
    errors.defense = "Defense isn't valid";
  }

  if ((power && power < 1) || power > 100 || isNaN(Number(power))) {
    errors.power = "Power isn't valid";
  }

  return Object.values(errors).length ? errors : false;
}
exports.validateFighter = validateFighter;
