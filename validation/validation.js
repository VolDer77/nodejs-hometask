const { validateUser } = require("./userValidation");
const { validateFighter } = require("./fighterValidation");

const validate = (name, params, method) => {
  switch (name) {
    case "user":
      return validateUser(params, method);
    case "fighter":
      return validateFighter(params, method);
    default:
      return;
  }
};

exports.validate = validate;
