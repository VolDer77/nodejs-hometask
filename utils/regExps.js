exports.REG_EXPS = {
  GMAIL_REGEXP: new RegExp(/^[a-z0-9](\.?[a-z0-9]){5,}@gmail\.com$/),
  PHONE_NUMBER_REGEXP: new RegExp(/^\+?3?8?(0\d{9})$/),
};
