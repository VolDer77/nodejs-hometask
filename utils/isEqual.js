const isEqual = (model, params) => {
  if (model.hasOwnProperty("id")) {
    delete model.id;
  }
  let modelsKeys = Object.keys(model).sort();
  if (model.hasOwnProperty("health") && !params.hasOwnProperty("health")) {
    const elemToDelete = modelsKeys.indexOf("health");
    modelsKeys.splice(elemToDelete, 1);
  }
  const paramsKeys = Object.keys(params).sort();

  if (modelsKeys.length !== paramsKeys.length) {
    return false;
  }

  return modelsKeys.every((key, idx) => key === paramsKeys[idx]);
};
exports.isEqual = isEqual;
